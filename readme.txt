

 
 Description
 -----------

 Block descriptions let you attach descriptions to the block list in the user edit form. 


 Installation
 ------------

 Install as any other drupal module. If you need more information please visit the link:

 "Installing contributed modules" - http://drupal.org/node/70151


 Configuration
 -------------

 This module has no any configuration interface. 


 Use
 ---

 Drupal blocks have the feature that can be set visible or hidden by the users. To configure
 this option the site administrator should configure the modules for this, hidding or being shown 
 by default and letting users to alter this property. 

 In the blocks page (http://site/admin/build/block), near each block there's an option to configure 
 settings for it. For each block, the form will show the option to hide or show this block by default.
 This module includes a textarea for the admin to include a little description for this block. This 
 description is shown close to the checkbox in the user configuration form to let the user know the
 purpose of each block.

 You may also set the description while creating the box from the main block administration page.

 